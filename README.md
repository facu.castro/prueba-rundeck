<h2><strong>Links</strong></h2>
<div><strong>Helm chart Rundeck:</strong> <a href="https://artifacthub.io/packages/helm/dwardu-helm-charts/rundeck" target="_blank" rel="nofollow noreferrer noopener">https://artifacthub.io/packages/helm/dwardu-helm-charts/rundeck</a></div>
<div><strong>Rundeck dockerfile oficial:</strong> <a href="https://github.com/rundeck/rundeck/tree/main/docker/official" target="_blank" rel="nofollow noreferrer noopener">https://github.com/rundeck/rundeck/tree/main/docker/official</a></div>
<div><strong>Templates Helm chart:</strong> <a href="https://github.com/dwardu89/helm-charts/tree/master/charts/rundeck/templates" target="_blank" rel="nofollow noreferrer noopener">https://github.com/dwardu89/helm-charts/tree/master/charts/rundeck/templates</a></div>
<p>&nbsp;</p>
<h2>Pasos seguidos</h2>
<ol>
<li>
<h4>Levantamos rundeck con helm</h4>
<br /><code class="language-bash" style="text-align: center;">helm install rundeck dwardu-helm-charts/rundeck -n rundeck --create-namespace</code><br /><br /><br />Los cm de los templates toman los valores de <a href="https://github.com/dwardu89/helm-charts/blob/master/charts/rundeck/values.yaml">values.yaml</a>. Hay que editar de ah&iacute; las variables correspondientes a la DB.<br /><br /><br /></li>
<li>
<h4>Deployamos el Postgres</h4>
Con la linea <br /><br /><code class="language-bash">kubectl edit configmap rundeck-environment-configmap -n rundeck</code><br /><br /></li>
<li>
<h4>Probamos conectarnos al container de rundeck</h4>
<code class="language-bash">kubectl exec [POD NAME] -c rundeck -n rundeck -- /bin/bash</code><br /><br /><code class="language-bash">kubectl exec [POD NAME] -c rundeck -n rundeck -- /bin/sh</code><br /><br />La terminal no devolvio nada.<br /><br /></li>
<li>
<h4>Agregamos las variables:</h4>
# RUNDECK_DATABASE_DRIVER: org.postgresql.Driver<br /># RUNDECK_DATABASE_USERNAME: rundeck<br /># RUNDECK_DATABASE_PASSWORD: rundeck<br /><br />al configmap del rundeck que ya estaba iniciado, lo destruimos para reiniciar pero ahora crashea por que la variable RUNDECK_DATABASE_URL est&aacute; mal configurada.<br /><br />Docu de la variable: <a href="https://github.com/rundeck/rundeck/tree/main/docker/official#rundeck_database_url">https://github.com/rundeck/rundeck/tree/main/docker/official#rundeck_database_url</a></li>
</ol>
